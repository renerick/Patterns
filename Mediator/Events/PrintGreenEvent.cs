namespace Mediator.Events
{
    public class PrintGreenEvent : IEvent
    {
        public string Text { get; set; }
    }
}