using System;

namespace Mediator.Events
{
    public class SecondPrintEventHandler : IEventHandler<PrintEvent>
    {
        public void Handle(PrintEvent command)
        {
            Console.WriteLine($"Second PrintEvent handler triggered, text: {command.Text}");
        }
    }
}