namespace Mediator.Events
{
    public class PrintEvent : IEvent
    {
        public string Text { get; set; }
    }
}