using System;

namespace Mediator.Events
{
    public class PrintGreenEventHandler : IEventHandler<PrintGreenEvent>
    {
        public void Handle(PrintGreenEvent command)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"PrintGreenEvent handler triggered, text: {command.Text}");
            Console.ResetColor();
        }
    }
}