using System;

namespace Mediator.Events
{
    public class FirstPrintEventHandler : IEventHandler<PrintEvent>
    {
        public void Handle(PrintEvent command)
        {
            Console.WriteLine($"First PrintEvent handler triggered, text: {command.Text}");
        }
    }
}