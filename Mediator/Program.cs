﻿using System;
using System.Linq;
using Mediator.Events;

namespace Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            var mediator = new Mediator();
            mediator.SetHandler(new PrintCommandHandler());
            mediator.SetHandler(new PrintGreenCommandHandler());
            mediator.Subscribe(new FirstPrintEventHandler());
            mediator.Subscribe(new FirstPrintEventHandler());
            mediator.Subscribe(new SecondPrintEventHandler());
            mediator.Subscribe(new PrintGreenEventHandler());

            string input = Console.ReadLine();
            while (input != "exit")
            {
                var tokens = input?.Split();
                var text = string.Join(' ', tokens.Skip(1));
                switch (tokens[0])
                {
                    case "print":
                        mediator.Execute(new PrintCommand() { Text = text });
                        mediator.Publish(new PrintEvent() { Text = text });
                        break;
                    case "printGreen":
                        mediator.Execute(new PrintGreenCommand() { Text = text });
                        mediator.Publish(new PrintEvent() { Text = text });
                        mediator.Publish(new PrintGreenEvent() { Text = text });
                        break;
                    default:
                        Console.WriteLine("Unknown command");
                        break;
                }

                input = Console.ReadLine();
            }
        }
    }
}