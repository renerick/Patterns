namespace Mediator
{
    public class PrintGreenCommand : ICommand
    {
        public string Text { get; set; }
    }
}