using System;
using Mediator.Events;

namespace Mediator
{
    public class PrintGreenCommandHandler : ICommandHandler<PrintGreenCommand>
    {
        public void Execute(PrintGreenCommand command)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(command.Text);
            Console.ResetColor();
        }
    }
}