namespace Mediator
{
    public class PrintCommand : ICommand
    {
        public string Text { get; set; }
    }
}