using System;

namespace Mediator
{
    public class PrintCommandHandler : ICommandHandler<PrintCommand>
    {
        public void Execute(PrintCommand command)
        {
            Console.WriteLine(command.Text);
        }
    }
}