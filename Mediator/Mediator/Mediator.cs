using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace Mediator
{
    public class Mediator : IMediator
    {
        private readonly Dictionary<Type, ICommandHandler> _commandHandlers;
        private readonly Dictionary<Type, IList<IEventHandler>> _eventHandlers;

        public Mediator()
        {
            _commandHandlers = new Dictionary<Type, ICommandHandler>();
            _eventHandlers = new Dictionary<Type, IList<IEventHandler>>();
        }

        public void Execute<TCommand>(TCommand command) where TCommand : ICommand
        {
            if (!_commandHandlers.TryGetValue(command.GetType(), out var handler))
            {
                throw new Exception($"No handler for command {command}");
            }

            var resultHandler = (ICommandHandler<TCommand>) handler;
            resultHandler.Execute(command);
        }

        public void Publish<TEvent>(TEvent @event) where TEvent : IEvent
        {
            if (!_eventHandlers.TryGetValue(@event.GetType(), out var handlers))
            {
                return;
            }

            foreach (var eventHandler in handlers)
            {
                ((IEventHandler<TEvent>) eventHandler).Handle(@event);
            }
        }

        public void Subscribe<TEvent>(IEventHandler<TEvent> handler) where TEvent : IEvent => Subscribe(typeof(TEvent), handler);

        public void Subscribe(Type @event, IEventHandler handler)
        {
            if (!typeof(IEvent).IsAssignableFrom(@event))
            {
                throw new Exception("type must be derivative of IEvent");
            }

            var genericHandler = typeof(IEventHandler<>).MakeGenericType(@event);

            if (!genericHandler.IsAssignableFrom(handler.GetType()))
            {
                throw new Exception($"Handler must be derivative from IEventHandler<{@event.Name}>");
            }

            if (_eventHandlers.TryGetValue(@event, out var handlers))
            {
                handlers.Add(handler);
            }
            else
            {
                _eventHandlers.Add(@event, new List<IEventHandler> { handler });
            }
        }

        public void UnSubscribe<TEvent>(IEventHandler<TEvent> handler) where TEvent : IEvent
        {
            UnSubscribe(typeof(TEvent), handler);
        }

        public void UnSubscribe(Type @event, IEventHandler handler)
        {
            if (!_eventHandlers.TryGetValue(@event, out var handlers)) return;

            handlers.Remove(handler);
            if (handlers.Count == 0)
            {
                _eventHandlers.Remove(@event);
            }
        }

        public void SetHandler<TCommand>(ICommandHandler<TCommand> handler) where TCommand : ICommand
        {
            SetHandler(typeof(TCommand), handler);
        }

        public void SetHandler(Type command, ICommandHandler handler)
        {
            if (!typeof(ICommand).IsAssignableFrom(command))
            {
                throw new Exception("type must be derivative of ICommand");
            }

            var genericHandler = typeof(ICommandHandler<>).MakeGenericType(command);

            if (!genericHandler.IsInstanceOfType(handler))
            {
                throw new Exception($"Handler must be derivative from ICommandHandler<{command.Name}>");
            }

            if (_commandHandlers.ContainsKey(command))
            {
                throw new Exception("Handler already defined");
            }

            _commandHandlers.Add(command, handler);
        }

        public void RemoveHandler<TCommand>() where TCommand : ICommand
        {
            RemoveHandler(typeof(TCommand));
        }

        public void RemoveHandler(Type command)
        {
            _commandHandlers.Remove(command);
        }
    }
}