using System;

namespace Mediator
{
    public interface IMediator
    {
        void Execute<TCommand>(TCommand command) where TCommand : ICommand;
        void Publish<TEvent>(TEvent @event) where TEvent : IEvent;
        void Subscribe<TEvent>(IEventHandler<TEvent> handler) where TEvent : IEvent;
        void Subscribe(Type @event, IEventHandler handler);
        void UnSubscribe<TEvent>(IEventHandler<TEvent> handler) where TEvent : IEvent;
        void UnSubscribe(Type @event, IEventHandler handler);
        void SetHandler<TCommand>(ICommandHandler<TCommand> handler) where TCommand : ICommand;
        void SetHandler(Type command, ICommandHandler handler);
        void RemoveHandler<TCommand>() where TCommand : ICommand;
        void RemoveHandler(Type command);
    }
}