namespace FlyWeight
{
    public class GameCharacter
    {
        public GameCharacterProperties Properties { get; set; }

        public int Health { get; set; }
        public int AttackModifier { get; set; }
        public int DefenceModifier { get; set; }
    }
}