namespace FlyWeight
{
    public class GameCharacterProperties
    {
        public string Name { get; set; }
        public int MaxHealth { get; set; }
        public int BaseAttack { get; set; }
        public int BaseDefence { get; set; }
        // Lets assume that this is base64 encoded image
        public string Texture { get; set; }
    }
}