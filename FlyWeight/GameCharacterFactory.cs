using System.Text;

namespace FlyWeight
{
    public class GameCharacterFactory
    {
        private readonly GameCharacterProperties MagicianProperties = new GameCharacterProperties()
        {
            Name = "Magician",
            Texture = "HUGE TEXTURE WITH GIGABYTES OF DATA 1",
            BaseAttack = 100,
            BaseDefence = 10,
            MaxHealth = 50,
        };

        private readonly GameCharacterProperties SkeletonProperties = new GameCharacterProperties()
        {
            Name = "Spooky scary skeleton",
            Texture = "HUGE TEXTURE WITH GIGABYTES OF DATA 2",
            BaseAttack = 80,
            BaseDefence = 60,
            MaxHealth = 80,
        };

        private readonly GameCharacterProperties ZombieProperty = new GameCharacterProperties()
        {
            Name = "Zombie",
            Texture = "HUGE TEXTURE WITH GIGABYTES OF DATA 3",
            BaseAttack = 20,
            BaseDefence = 30,
            MaxHealth = 100,
        };

        public GameCharacter CreateMagician()
        {
            return new GameCharacter()
            {
                Properties = MagicianProperties,
                Health = MagicianProperties.MaxHealth,
                AttackModifier = 0,
                DefenceModifier = 0,
            };
        }

        public GameCharacter CreateSkeleton()
        {
            return new GameCharacter()
            {
                Properties = SkeletonProperties,
                Health = SkeletonProperties.MaxHealth,
                AttackModifier = 0,
                DefenceModifier = 0,
            };
        }

        public GameCharacter CreateZombie()
        {
            return new GameCharacter()
            {
                Properties = ZombieProperty,
                Health = ZombieProperty.MaxHealth,
                AttackModifier = 0,
                DefenceModifier = 0,
            };
        }
    }
}