﻿using System;
using System.Collections.Generic;

namespace FlyWeight
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new GameCharacterFactory();
            var enemies = new List<GameCharacter>();
            for (int i = 0; i < 1000; i++)
            {
                enemies.Add(factory.CreateZombie());
            }
            for (int i = 0; i < 100; i++)
            {
                enemies.Add(factory.CreateMagician());
            }
            for (int i = 0; i < 10000; i++)
            {
                enemies.Add(factory.CreateSkeleton());
            }
        }
    }
}