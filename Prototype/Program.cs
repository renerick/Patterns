﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            var workerPrototype1 = new Worker((sender, eventArgs) => Console.WriteLine(Guid.NewGuid()), TimeSpan.FromSeconds(1));
            var workerPrototype2 = new Worker((sender, eventArgs) => Console.WriteLine(Guid.NewGuid() + "111---111"), TimeSpan.FromSeconds(2));

            var workers = new List<Worker>(100);
            for (int i = 0; i < 100; i++)
            {
                workers.Add((Worker) workerPrototype1.Clone());
                workers.Add((Worker) workerPrototype2.Clone());
            }

            foreach (var worker in workers)
            {
                worker.Start();
            }

            Thread.Sleep(10000);

            foreach (var worker in workers)
            {
                worker.Stop();
            }
        }
    }
}