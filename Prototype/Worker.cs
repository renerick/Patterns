using System;
using System.Timers;

namespace Prototype
{
    public class Worker : ICloneable
    {
        private Timer _timer;
        private ElapsedEventHandler _work;
        private TimeSpan _interval;

        public Worker(ElapsedEventHandler work, TimeSpan interval)
        {
            _interval = interval;
            _work = work;
        }

        public Worker(Worker worker)
        {
            _interval = worker._interval;
            _work = worker._work;
        }

        public void Start()
        {
            _timer = new Timer(_interval.TotalMilliseconds);
            _timer.Elapsed += _work;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
            _timer.Dispose();
        }

        public object Clone()
        {
            return new Worker(this);
        }
    }
}